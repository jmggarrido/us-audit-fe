'use strict';

/**
 * @ngdoc function
 * @name usAuditFeApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the usAuditFeApp
 */
angular.module('usAuditFeApp')
  .controller('ApisCtrl', function ($scope, NgTableParams, ApisFactory) {
      $scope.domain = domain;
      ApisFactory.getAccepted().then(function(data) {
        $scope.tableParams = new NgTableParams({}, {dataset: data.data });
      })
  });

