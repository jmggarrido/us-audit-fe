angular.module('usAuditFeApp')
  .factory('GeneralFactory', function($http, $q) {
      var factory = {};

      factory.getAccepted = function() {
        return $http.get(domain + '/general');
      }

      return factory;
  })
  .factory('ApisFactory', function($http, $q) {
      var factory = {};

      factory.getAccepted = function() {
        return $http.get(domain + '/apis');
      }

      factory.getTerm = function(termId, language) {
        return $http.get(domain + '/termReader?termId=' + termId + "&language=" + language);
      }

      return factory;
  })
