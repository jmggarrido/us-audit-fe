'use strict';

/**
 * @ngdoc function
 * @name usAuditFeApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the usAuditFeApp
 */
angular.module('usAuditFeApp')
  .controller('MainCtrl', function ($scope, NgTableParams, GeneralFactory) {
      $scope.domain = domain;

      GeneralFactory.getAccepted().then(function(data) {
        $scope.tableParams = new NgTableParams({}, {dataset: data.data });
      })
  })
