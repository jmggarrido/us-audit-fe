angular.module('usAuditFeApp')
.filter('getFilename', function() {

    return function (file) {
      var parts = file.split("/");
      var fileName = parts[parts.length - 1];
      return fileName;
    }
  });
