'use strict';

/**
 * @ngdoc function
 * @name usAuditFeApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the usAuditFeApp
 */
angular.module('usAuditFeApp')
  .controller('ApisHtmlCtrl', function ($scope, $routeParams, $sce, ApisFactory) {
      $scope.termId = $routeParams.termId;
      $scope.language = $routeParams.language;

      ApisFactory.getTerm($scope.termId, $scope.language).then(function(data) {
        $scope.terms = $sce.trustAsHtml($scope.language == "en" ? data.data.htmlEn : data.data.htmlEs);
      });
  });

