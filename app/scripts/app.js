'use strict';
var domain = "http://localhost:3000";
/**
 * @ngdoc overview
 * @name usAuditFeApp
 * @description
 * # usAuditFeApp
 *
 * Main module of the application.
 */
angular
  .module('usAuditFeApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'ngTable'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
      })
      .when('/apis', {
        templateUrl: 'views/apis.html',
        controller: 'ApisCtrl'
      })
      .when('/apisHtml/:termId/:language', {
        templateUrl: 'views/apisHtml.html',
        controller: 'ApisHtmlCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
